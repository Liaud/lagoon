use std::ops::{Deref, DerefMut, Index, IndexMut};
use std::marker::PhantomData;
use serde::{Serialize, Serializer, Deserialize, Deserializer};
use serde::de;
use serde::ser::impls::MapIteratorVisitor;
use vm;

#[derive(Debug, Clone)]
pub struct VecMap<V>(vm::VecMap<V>);

impl<V> VecMap<V> {
    pub fn new() -> VecMap<V> {
        VecMap(vm::VecMap::new())
    }

    pub fn with_capacity(capacity: usize) -> VecMap<V> {
        VecMap(vm::VecMap::with_capacity(capacity))
    }
}

impl<V> Deref for VecMap<V> {
    type Target = vm::VecMap<V>;

    fn deref(&self) -> &vm::VecMap<V> {
        &self.0
    }
}

impl<V> DerefMut for VecMap<V> {
    fn deref_mut(&mut self) -> &mut vm::VecMap<V> {
        &mut self.0
    }
}

impl<V> Index<usize> for VecMap<V> {
    type Output = V;

    #[inline]
    fn index(&self, index: usize) -> &V {
        self.0.index(index)
    }
}

impl<V> IndexMut<usize> for VecMap<V> {
    #[inline]
    fn index_mut(&mut self, index: usize) -> &mut V {
        self.0.index_mut(index)
    }
}

impl<V: Serialize> Serialize for VecMap<V> {
    fn serialize<S: Serializer>(&self, serializer: &mut S) -> Result<(), S::Error> {
        serializer.visit_map(MapIteratorVisitor::new(self.iter(), Some(self.len())))
    }
}

impl<V: Deserialize> Deserialize for VecMap<V> {
    fn deserialize<D: Deserializer>(deserializer: &mut D) -> Result<VecMap<V>, D::Error> {
        deserializer.visit(VecMapVisitor::new())
    }
}

struct VecMapVisitor<V> {
    marker: PhantomData<VecMap<V>>,
}

impl<V> VecMapVisitor<V> {
    pub fn new() -> VecMapVisitor<V> {
        VecMapVisitor { marker: PhantomData }
    }
}

impl<V: Deserialize> de::Visitor for VecMapVisitor<V> {
    type Value = VecMap<V>;

    fn visit_unit<E: de::Error>(&mut self) -> Result<VecMap<V>, E> {
        Ok(VecMap::new())
    }

    fn visit_map<V_: de::MapVisitor>(&mut self, mut visitor: V_) -> Result<VecMap<V>, V_::Error> {
        let mut values = VecMap::new();

        while let Some((index, value)) = try!(visitor.visit()) {
            values.insert(index, value);
        }
        try!(visitor.end());

        Ok(values)
    }
}
