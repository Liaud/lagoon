use bit_set::BitSet;
use std::any::TypeId;
use std::collections::HashMap;
use std::sync::RwLock;
use std::sync::atomic::{AtomicUsize, Ordering};

pub type TypeIndex = usize;

#[derive(Debug)]
pub struct TypeMap {
    id_to_index: RwLock<HashMap<TypeId, TypeIndex>>,
    index_to_id: RwLock<HashMap<TypeIndex, TypeId>>,
    next: AtomicUsize,
}

impl TypeMap {
    pub fn new() -> TypeMap {
        TypeMap {
            id_to_index: RwLock::new(HashMap::new()),
            index_to_id: RwLock::new(HashMap::new()),
            next: AtomicUsize::new(0),
        }
    }

    pub fn register(&self, type_id: TypeId) -> TypeIndex {
        let type_index = self.next.fetch_add(1, Ordering::SeqCst);
        let mut id_to_index = self.id_to_index.write().unwrap();
        let mut index_to_id = self.index_to_id.write().unwrap();

        id_to_index.insert(type_id, type_index);
        index_to_id.insert(type_index, type_id);

        type_index
    }

    pub fn has(&self, type_id: TypeId) -> bool {
        let id_to_index = self.id_to_index.read().unwrap();
        id_to_index.contains_key(&type_id)
    }

    pub fn type_id_from_index(&self, index: TypeIndex) -> TypeId {
        let index_to_id = self.index_to_id.read().unwrap();
        index_to_id[&index]
    }

    pub fn type_index_from_id(&self, type_id: TypeId) -> TypeIndex {
        let id_to_index = self.id_to_index.read().unwrap();
        id_to_index[&type_id]
    }
}

pub type TypeIndexSet = BitSet;
