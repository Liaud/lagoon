extern crate serde;
extern crate vec_map as vm;
extern crate bit_set;

pub mod vec_map;
pub mod type_map;
