use utils::type_map::TypeMap;
use component::Component;
use std::any::TypeId;
use tag::Tag;

pub use utils::type_map::TypeIndex;

lazy_static! {
    static ref TYPE_MAP: TypeMap = TypeMap::new();
}

macro_rules! type_index {
    ($name:ident, $bound:ident, $map:expr) => (
        #[derive(Clone, Copy, Eq, PartialEq, Hash, Debug)]
        pub struct $name(TypeId);

        impl $name {
            pub fn of<C: $bound>() -> $name {
                $name(TypeId::of::<C>())
            }

            pub fn index<C: $bound>() -> ::utils::type_map::TypeIndex {
                let id = TypeId::of::<C>();

                if !$map.has(id) {
                    $map.register(id)
                } else {
                    $map.type_index_from_id(id)
                }
            }

            pub fn from_index(type_index: ::utils::type_map::TypeIndex) -> $name {
                $name($map.type_id_from_index(type_index))
            }

            pub fn as_index(&self) -> ::utils::type_map::TypeIndex {
                if !$map.has(self.0) {
                    $map.register(self.0)
                } else {
                    $map.type_index_from_id(self.0)
                }
            }
        }
    );
}

type_index!(ComponentId, Component, TYPE_MAP);
type_index!(TagId, Tag, TYPE_MAP);
