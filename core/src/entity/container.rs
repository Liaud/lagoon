use entity::{EntityHandle, EntityIndex, Entity};
use policy;

type Link = usize;

/// The container of the game entities.
#[derive(Debug, Clone)]
pub struct EntityContainer {
    entities: Vec<Entity>,
    links: Vec<Link>,
    len: usize,
}

impl EntityContainer {
    /// Create a new bag
    pub fn new() -> EntityContainer {
        EntityContainer {
            entities: Vec::new(),
            links: Vec::new(),
            len: 0,
        }
    }

    /// Creates a new entity and returns its handle
    pub fn create(&mut self) -> EntityIndex {
        if self.len == self.entities.capacity() {
            self.grow();
        }

        let index = self.entities[self.len].index();
        self.len += 1;

        index
    }

    /// Returns number of entities alives
    pub fn len(&self) -> usize {
        self.len
    }

    fn grow(&mut self) {
        let old_capacity = self.entities.capacity();
        let new_capacity = self.next_capacity()
                               .expect("Maximum entity count reached.");

        let amount = new_capacity - old_capacity;

        self.entities.reserve_exact(amount);
        self.links.reserve_exact(amount);

        for i in old_capacity..new_capacity {
            let handle = EntityHandle::new(policy::id_from_usize(i), 0);

            self.entities.push(Entity::new(handle));
            self.links.push(i);
        }
    }

    fn next_capacity(&self) -> Option<usize> {
        let current_capacity = self.entities.capacity();
        let additional_amount = (current_capacity + 10) * 2;
        let max_bound = policy::max_entity_count();


        let new_capacity = current_capacity + additional_amount;
        if new_capacity > max_bound {
            Some(max_bound)
        } else if new_capacity <= max_bound {
            Some(new_capacity)
        } else {
            None
        }
    }

    /// Removes an entity
    pub fn remove(&mut self, index: EntityIndex) -> bool {
        if self.get(index).is_none() {
            return false;
        }

        let last_entity = self.len - 1;
        let last_index = self.entities[last_entity].index();
        self.links.swap(index, last_index);

        self.entities.swap(self.links[last_index], self.links[index]);

        let mut removed = &mut self.entities[self.links[index]];
        removed.handle.version.wrapping_add(1);
        removed.clear_attached_components();
        self.len -= 1;

        true
    }

    /// Try to return the entity from the handle
    pub fn get(&self, index: EntityIndex) -> Option<&Entity> {
        let link = self.links[index];

        self.entities.get(link).and_then(|entity| {
            if link < self.len {
                Some(entity)
            } else {
                None
            }
        })
    }

    /// Try to return a mutable entity from the handle
    pub fn get_mut(&mut self, index: EntityIndex) -> Option<&mut Entity> {
        let link = self.links[index];
        let len = self.len;

        self.entities.get_mut(link).and_then(|entity| {
            if link < len {
                Some(entity)
            } else {
                None
            }
        })
    }

    /// Return all the entity alives
    pub fn alives(&self) -> &[Entity] {
        &self.entities[0..self.len]
    }
}
