use bincode::SizeLimit;
use bincode::serde::{serialized_size, serialize_into};
use component::{Component, ComponentId};
use std::slice;
use type_index::TagId;
use tag::Tag;

type ComponentByteSize = u64;

#[derive(Clone)]
pub struct EntityPrototype {
    types: Vec<(ComponentId, ComponentByteSize)>,
    tags: Vec<TagId>,
    components: Vec<u8>,
}

impl EntityPrototype {
    pub fn new() -> EntityPrototype {
        EntityPrototype {
            types: Vec::new(),
            tags: Vec::new(),
            components: Vec::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> EntityPrototype {
        EntityPrototype {
            types: Vec::new(),
            tags: Vec::new(),
            components: Vec::with_capacity(capacity),
        }
    }

    pub fn attach_component<C: Component>(&mut self, component: C) {
        self.types.push((ComponentId::of::<C>(), serialized_size(&component)));
        serialize_into(&mut self.components, &component, SizeLimit::Infinite)
            .expect("Failed to serialize component into prototype.");
    }

    pub fn tag<T: Tag>(&mut self) {
        self.tags.push(TagId::of::<T>());
    }

    pub fn components(&self) -> Iter {
        Iter {
            types: self.types.iter(),
            byte_position: 0,
            components: &self.components,
        }
    }

    pub fn tags(&self) -> &[TagId] {
        &self.tags
    }
}

pub struct Iter<'a> {
    types: slice::Iter<'a, (ComponentId, ComponentByteSize)>,
    byte_position: usize,
    components: &'a [u8],
}

impl<'a> Iterator for Iter<'a> {
    type Item = (ComponentId, &'a [u8]);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(&(id, size)) = self.types.next() {
            let data = &self.components[self.byte_position..(size as usize)];
            self.byte_position += size as usize;

            return Some((id, data));
        }
        None
    }
}
