use utils::type_map::TypeIndexSet;
use type_index::TypeIndex;
use entity::{Entity, EntityManager};

#[derive(Debug, Clone)]
pub struct Filter {
    accept: TypeIndexSet,
    reject: TypeIndexSet,
}

impl Filter {
    pub fn new() -> Filter {
        Filter {
            accept: TypeIndexSet::new(),
            reject: TypeIndexSet::new(),
        }
    }

    pub fn accept(&mut self, type_index: TypeIndex) {
        self.accept.insert(type_index);
    }

    pub fn reject(&mut self, type_index: TypeIndex) {
        self.reject.insert(type_index);
    }

    pub fn matches(&self, indexes: &TypeIndexSet) -> bool {
        indexes.is_superset(&self.accept) && indexes.is_disjoint(&self.reject)
    }

    pub fn iter_on<'a>(&'a self, entities: &'a EntityManager) -> Iter<'a> {
        Iter {
            entities: entities.alives().iter(),
            filter: self,
        }
    }
}

pub struct Iter<'a> {
    entities: ::std::slice::Iter<'a, Entity>,
    filter: &'a Filter,
}

impl<'a> Iterator for Iter<'a> {
    type Item = &'a Entity;

    fn next(&mut self) -> Option<&'a Entity> {
        while let Some(entity) = self.entities.next() {
            if self.filter.matches(entity.attached_indexes()) {
                return Some(entity);
            }
        }
        None
    }
}


#[macro_export]
macro_rules! filter {
    (
        all_components: [ $($accepted_components:ty),* ],
        all_tags: [ $($accepted_tags:ty),* ],
        none_components: [ $($rejected_components:ty),* ],
        none_tags: [ $($rejected_tags:ty),* ]
    ) => (
        {
            let mut result = $crate::entity::Filter::new();

            $(
                result.accept(ComponentId::index::<$accepted_components>());
            )*
            $(
                result.accept(TagId::index::<$accepted_tags>());
            )*
            $(
                result.reject(ComponentId::index::<$rejected_components>());
            )*
            $(
                result.reject(TagId::index::<$rejected_tags>());
            )*

            result
        }
    );
}
