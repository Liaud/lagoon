pub mod container;
pub mod prototype;
pub mod manager;
pub mod filter;

pub use self::container::EntityContainer;
pub use self::prototype::EntityPrototype;
pub use self::manager::EntityManager;
pub use self::filter::Filter;

use policy::{Id, Version};
use component::Container;
use serde::{Serializer, Deserializer};
use utils::type_map::{TypeIndex, TypeIndexSet};

pub type EntityIndex = usize;

/// An Entity is defined by its attached components
#[derive(Debug, Clone)]
pub struct Entity {
    handle: EntityHandle,
    attached_components: TypeIndexSet,
}

impl Entity {
    /// Creates a new entity
    fn new(handle: EntityHandle) -> Entity {
        Entity {
            handle: handle,
            attached_components: TypeIndexSet::new(),
        }
    }

    /// Returns the entity handle
    #[inline]
    pub fn handle(&self) -> EntityHandle {
        self.handle
    }

    /// Returns the entity version
    #[inline]
    pub fn version(&self) -> Version {
        self.handle.version()
    }

    /// Returns the entity index
    #[inline]
    pub fn index(&self) -> EntityIndex {
        self.handle.index()
    }

    fn attach(&mut self, index: TypeIndex) {
        self.attached_components.insert(index);
    }

    fn detach(&mut self, index: TypeIndex) {
        self.attached_components.remove(index);
    }

    fn clear_attached_components(&mut self) {
        self.attached_components.clear();
    }

    pub fn attached_indexes(&self) -> &TypeIndexSet {
        &self.attached_components
    }
}

/// Represents an entity. All reference to any entity should use this handle
/// This handle is also the primary key to access components
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct EntityHandle {
    id: Id,
    version: Version,
}

impl EntityHandle {
    /// Creates a new handle
    fn new(index: Id, version: Version) -> EntityHandle {
        EntityHandle {
            id: index,
            version: version,
        }
    }

    /// Returns the index of the handle
    #[inline]
    fn index(&self) -> EntityIndex {
        self.id as usize
    }

    /// Returns the version of the handle
    #[inline]
    fn version(&self) -> Version {
        self.version
    }
}
