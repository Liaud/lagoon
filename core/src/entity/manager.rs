use entity::{EntityContainer, Entity, EntityIndex, EntityHandle};
use component::ComponentId;
use tag::TagId;
use utils::type_map::TypeIndexSet;

#[derive(Debug)]
pub struct EntityManager {
    container: EntityContainer,
}

impl EntityManager {
    pub fn new() -> EntityManager {
        EntityManager { container: EntityContainer::new() }
    }

    pub fn create(&mut self) -> EntityIndex {
        self.container.create()
    }

    pub fn remove(&mut self, entity: EntityIndex) {
        self.container.remove(entity);
    }

    pub fn attach_component(&mut self, entity: EntityIndex, component_id: ComponentId) {
        if let Some(entity) = self.container.get_mut(entity) {
            entity.attach(component_id.as_index())
        }
    }

    pub fn detach_component(&mut self, entity: EntityIndex, component_id: ComponentId) {
        if let Some(entity) = self.container.get_mut(entity) {
            entity.detach(component_id.as_index())
        }
    }

    pub fn attached_indexes(&self, entity: EntityIndex) -> Option<&TypeIndexSet> {
        self.container.get(entity).map(|entity| entity.attached_indexes())
    }

    pub fn tag(&mut self, entity: EntityIndex, tag_id: TagId) {
        if let Some(entity) = self.container.get_mut(entity) {
            entity.attach(tag_id.as_index())
        }
    }

    pub fn untag(&mut self, entity: EntityIndex, tag_id: TagId) {
        if let Some(entity) = self.container.get_mut(entity) {
            entity.detach(tag_id.as_index())
        }
    }

    pub fn alives(&self) -> &[Entity] {
        self.container.alives()
    }

    pub fn handle(&self, entity: EntityIndex) -> EntityHandle {
        let entity = self.container.get(entity).expect("entity requested is dead");
        entity.handle()
    }

    pub fn updgrade_handle(&self, handle: EntityHandle) -> Option<EntityIndex> {
        self.container.get(handle.index()).and_then(|entity| {
            if entity.version() == handle.version() {
                return Some(handle.index())
            }
            None
        })
    }
}
