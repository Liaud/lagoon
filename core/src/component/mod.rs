pub mod container;

pub use self::container::{Container, PackedContainer};
pub use type_index::ComponentId;

use std::any::Any;
use std::fmt::Debug;
use serde::{Serialize, Deserialize};

pub trait Component: Any + Debug + Clone + Send + Serialize + Deserialize {
    type Container: Container<Value = Self>;
}
