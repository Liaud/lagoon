use component::{Component, Container};
use entity::EntityIndex;
use utils::vec_map::VecMap;

#[derive(Clone, Debug, Serialize, Deserialize)]
struct ComponentEntry<C: Component> {
    link_index: usize,
    component: C,
}

impl<C: Component> ComponentEntry<C> {
    pub fn new(link_index: usize, component: C) -> ComponentEntry<C> {
        ComponentEntry {
            link_index: link_index,
            component: component,
        }
    }
}

type Link = usize;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PackedContainer<C: Component> {
    components: Vec<ComponentEntry<C>>,
    links: VecMap<Link>,
}

impl<C: Component> PackedContainer<C> {
    pub fn new() -> PackedContainer<C> {
        PackedContainer {
            components: Vec::new(),
            links: VecMap::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> PackedContainer<C> {
        PackedContainer {
            components: Vec::with_capacity(capacity),
            links: VecMap::with_capacity(capacity),
        }
    }
}

impl<C: Component> Container for PackedContainer<C> {
    type Value = C;

    fn insert(&mut self, index: EntityIndex, component: C) {
        let entry = ComponentEntry::new(index, component);

        if let Some(&link) = self.links.get(&index) {
            self.components[link] = entry;
            return;
        }

        let link = self.components.len();
        self.links.insert(index, link);
        self.components.push(entry);
    }

    fn has(&self, index: EntityIndex) -> bool {
        return self.get(index).is_some();
    }

    fn get(&self, index: EntityIndex) -> Option<&C> {
        if let Some(&link) = self.links.get(&index) {
            return self.components.get(link).map(|entry| &entry.component);
        }
        None
    }

    fn get_mut(&mut self, index: EntityIndex) -> Option<&mut C> {
        if let Some(&link) = self.links.get(&index) {
            return self.components.get_mut(link).map(|entry| &mut entry.component);
        }
        None
    }

    fn remove(&mut self, index: EntityIndex) -> Option<C> {
        let links = &mut self.links;
        if let Some(link) = links.remove(&index) {
            let entry = self.components.swap_remove(link);
            self.components.get(link).map(|entry| links.insert(entry.link_index, link));

            return Some(entry.component);
        }

        None
    }
}
