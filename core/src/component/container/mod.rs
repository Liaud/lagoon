pub mod packed;

use mopa::Any;
use std::fmt::Debug;
use entity::EntityIndex;
use component::Component;

pub use self::packed::PackedContainer;

pub trait Container: Any + Debug + Clone {
    type Value: Component;

    fn insert(&mut self, entity: EntityIndex, component: Self::Value);
    fn remove(&mut self, entity: EntityIndex) -> Option<Self::Value>;
    fn has(&self, entity: EntityIndex) -> bool;
    fn get(&self, entity: EntityIndex) -> Option<&Self::Value>;
    fn get_mut(&mut self, entity: EntityIndex) -> Option<&mut Self::Value>;
}
