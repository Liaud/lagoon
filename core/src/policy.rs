#[cfg(feature = "u16_handle")]
pub use self::u16_handle::*;

mod u16_handle {
    pub type Id = u16;
    pub type Version = u16;

    pub fn max_entity_count() -> usize {
        use std::u16::MAX;
        return MAX as usize;
    }

    #[inline]
    pub fn id_from_usize(value: usize) -> Id {
        value as u16
    }
}
