use std::any::Any;

pub use type_index::TagId;

pub trait Tag: Any {}

#[macro_export]
macro_rules! tags {
    ($($name:ident),*) => (
        $(
            #[derive(Copy, Clone, Debug)]
            pub struct $name;
            impl $crate::tag::Tag for $name {}
        )*
    );
}
