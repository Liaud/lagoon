#![feature(plugin, custom_derive)]
#![plugin(serde_macros)]

extern crate anymap;
extern crate lagoon_utils as utils;
extern crate serde;
extern crate bincode;
extern crate bit_set;

#[macro_use]
extern crate mopa;

#[macro_use]
extern crate lazy_static;

pub mod state;
pub mod component;
pub mod entity;
pub mod policy;
pub mod tag;
pub mod type_index;
pub mod write;
